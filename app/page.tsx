import { Button } from '@/components/ui/button'

export default function Home() {
	return (
		<main className="p-10">
			<Button variant="accent">Label</Button>
		</main>
	)
}
